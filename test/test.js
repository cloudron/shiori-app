#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');


describe('Application life cycle test', function () {
    this.timeout(0);

    const ADMIN_USERNAME = "shiori";
    const ADMIN_PASSWORD = "gopher";
 
    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    const BOOKMARK_ID = Math.floor((Math.random() * 100) + 1);
    const BOOKMARK_NAME = 'Cloudron ' + BOOKMARK_ID;
    const BOOKMARK_URL = 'https://cloudron.io/' + BOOKMARK_ID;

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function login(username, password) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}`);
        await browser.sleep(2000);

        await waitForElement(By.xpath('//input[@name="username"]'));
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//a[@class="button" and text()="Log In"]')).click();
        await browser.sleep(2000);

        await waitForElement(By.xpath(`//a[@title="Logout"]`));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn + '/#home');
        await browser.sleep(2000);
        await waitForElement(By.xpath(`//a[@title="Logout"]`));

        await browser.findElement(By.xpath('//a[@title="Logout"]')).click();
        await browser.sleep(2000);

        if (await browser.findElements(By.xpath('//p[@class="custom-dialog-content" and text()="Are you sure you want to log out ?"]')).then(found => !!found.length)) {
            await browser.findElement(By.xpath('//a[contains(@class, "custom-dialog-button")  and contains(text(), "Yes")]')).click();
            await browser.sleep(2000);
        }

        await waitForElement(By.xpath('//input[@name="username"]'));
    }

    async function createBookmark() {
        await browser.get('https://' + app.fqdn + '/#home');
        await browser.sleep(2000);
        await waitForElement(By.xpath('//a[@title="Add new bookmark"]'));
        await browser.findElement(By.xpath('//a[@title="Add new bookmark"]')).click();
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//input[contains(@placeholder, "Url, start with")]')).sendKeys(BOOKMARK_URL);
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//input[contains(@placeholder, "Custom title")]')).sendKeys(BOOKMARK_NAME);
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//a[contains(text(), "OK")]')).click();
        await browser.sleep(2000);

        await waitForElement(By.xpath('//p[@class="title" and contains(., "' + BOOKMARK_NAME + '")]'));
    }

    async function checkBookmark() {
        await browser.get('https://' + app.fqdn + '/#home');
        await browser.sleep(2000);

        await waitForElement(By.xpath('//p[@class="title" and contains(., "' + BOOKMARK_NAME + '")]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    it('install app', async function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        await browser.sleep(10000);
    });
    it('can get app info', getAppInfo);

    it('can Admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('create bookmark', createBookmark);

    it('can restart app', async function () {
        await browser.sleep(10000);
        execSync(`cloudron restart ${app.id}`);
    });

    it('can Admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('check bookmark', checkBookmark);
    it('can logout', logout);

    it('backup app', async function () {
        await browser.sleep(10000);
        execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS);
    });

    it('can Admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('check bookmark', checkBookmark);
    it('can logout', logout);


    it('restore app', async function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));

        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        await browser.sleep(10000);

        getAppInfo();

        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(10000);
    });

    it('can Admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('check bookmark', checkBookmark);
    it('can logout', logout);

    it('move to different location', async function () {
        browser.manage().deleteAllCookies();
        await browser.get('about:blank');

        execSync(`cloudron configure --location ${LOCATION}2`, EXEC_ARGS);
        await browser.sleep(10000);
        getAppInfo();
    });

    it('can Admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('check bookmark', checkBookmark);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app for update', async function () {
        execSync('cloudron install --appstore-id com.github.go_shiori --location ' + LOCATION, EXEC_ARGS);
        await browser.sleep(10000);
    });
    it('can get app info', getAppInfo);

    it('can Admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('create bookmark', createBookmark);
    it('can logout', logout);

    it('can update', async function () {
        await browser.sleep(10000);
        execSync(`cloudron update --app ${app.id}`, EXEC_ARGS);
    });

    it('can Admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('check bookmark', checkBookmark);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
