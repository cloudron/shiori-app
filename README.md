# Shiori-Cloudron

This is a custom package of Shiori https://github.com/go-shiori/shiori for deployment on Cloudron https://cloudron.io.

Please read `POSTINSTALL.md` for creating the first app user.

## Requirements

- already installed Cloudron on your VPS or a home server
- Docker installed on your development device
- Docker repository available (on Docker hub or your private Docker repo on Cloudron)
- Cloudron CLI installed on your development device and logged in

## Installation

- clone this repo to your development device
- cd into the directory
- manual process :
  - build the Dockerfile
  - upload the Docker image to your Docker repo
  - install to your Cloudron instance
- scripted process :
  - run ./cld.sh to do those 3 steps
  - e.g. ./cld.sh your-docker-repo/shiori-cloudron:tag
  - the script will prompt you for the `Location' : this is the subdomain for the app
    - e.g. shiori.domain.tld
