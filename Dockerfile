FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

# renovate: datasource=github-releases depName=go-shiori/shiori versioning=semver extractVersion=^v(?<version>.+)$
ARG SHIORI_VERSION=1.7.4

RUN curl -L https://github.com/go-shiori/shiori/releases/download/v${SHIORI_VERSION}/shiori_Linux_x86_64_${SHIORI_VERSION}.tar.gz | tar -zxvf - -C /app/code

COPY start.sh /app/code/start.sh

CMD ["/app/code/start.sh"]
