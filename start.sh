#!/bin/bash

set -eu

mkdir -p /app/data/data

# https://github.com/go-shiori/shiori/blob/master/docs/Configuration.md
export SHIORI_DIR=/app/data/data

echo "=> Changing ownership"
chown -R cloudron:cloudron /app/data

echo "=> Starting shiori"
exec gosu cloudron:cloudron /app/code/shiori serve
