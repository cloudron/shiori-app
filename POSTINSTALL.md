The initial credentials are:

**Username**: shiori<br/>
**Password**: gopher<br/>

Then, click the Settings gear icon and add an account.

**Important:** Once a new account is created, the default credentials will no longer work.

